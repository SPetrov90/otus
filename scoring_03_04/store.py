# -*- coding: utf-8 -*-

from functools import wraps
import logging
from time import sleep
import redis
from redis.exceptions import ConnectionError, TimeoutError


def check_connection(command):
    @wraps(command)
    def decorator(self, *args, **kwargs):
        try:
            return command(self, *args, **kwargs)
        except (AttributeError, ConnectionError, TimeoutError):
            self.make_connection()
            return command(self, *args, **kwargs)
    return decorator


class RedisStore(object):
    def __init__(self, host, port, retry=5, socket_timeout=2, cache_expire=3600, db=0, cache_prefix='cache'):
        self.host = host
        self.port = port
        self.connection = None
        self.retry_count = retry
        self.socket_timeout = socket_timeout
        self.cache_expire = cache_expire
        self.cache_prefix = cache_prefix
        self.db = db
        self.connect_attempts = 0

    def make_connection(self):
        while self.retry_count:
            try:
                self.connect_attempts += 1
                connection = redis.StrictRedis(host=self.host,
                                               port=self.port,
                                               socket_timeout=self.socket_timeout,
                                               decode_responses=True,
                                               db=self.db)
                self.connection = connection
                return True
            except Exception as error:
                logging.warning("Redis error: {}, retry {}...".format(error, self.retry_count))
                self.retry_count -= 1
                sleep(0.1)
        raise redis.exceptions.ConnectionError

    @check_connection
    def get(self, name):
        return self.connection.get(name)

    @check_connection
    def set(self, name, value):
        return self.connection.set(name, value)

    def cache_get(self, name):
        name = '{}_{}'.format(self.cache_prefix, name)
        try:
            return self.get(name)
        except Exception:
            return None

    def cache_set(self, name, data, cache_expire=None):
        name = '{}_{}'.format(self.cache_prefix, name)
        try:
            return self.connection.set(name, data, ex=cache_expire if cache_expire else self.cache_expire)
        except Exception:
            return None
