# API SCORING OTUS

### Requirements:
 - Python 2.7
 - redis (storage)
 - pytest (tests)
 - mock (tests)


## Usage:

#### Go to project dir:
```
cd %path_to_module_dir%
```

#### Run redis storage:

```
make redis
```
This command run redis server in docker container with publish 6379 port.

For stopping redis server use:

```
make redis_stop
```

#### Run api server:

```
make api
# or with args
make api [post='server_api_port'] [log='logfile']

```

## Tests:

For running tests use:
```
make test
```
