# -*- coding: utf-8 -*-

import abc
import datetime
import re

EMPTY_VALUES = ('', [], (), {}, None)
YEAR_DELTA_70 = datetime.timedelta(365 * 70 + (70 / 4), 0, 0)
DATE_FORMAT_RE = '%d.%m.%Y'
PHONE_RE = re.compile(r'^7[0-9]{10}$')
EMAIL_RE = re.compile(r'[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})', re.I | re.U)


class ValidationError(Exception):
    """ An error for validating data """

    def __init__(self, message):
        self.message = message


class Field(object):
    """
    Abstract field
    """
    __metaclass__ = abc.ABCMeta
    empty_values = list(EMPTY_VALUES)

    def __init__(self, required=True, nullable=True):
        self.required = required
        self.nullable = nullable
        self.label = None

    def __get__(self, obj, type=None):
        return obj.__dict__.get(self.label)

    def __set__(self, obj, value):
        if value is None and self.required:
            raise ValidationError('Value is required')
        value = self.to_python(value)
        if not self.nullable and value in self.empty_values:
            raise ValidationError('Value cant be blank')
        else:
            try:
                if value not in self.empty_values:
                    self.validate(value)
            except ValidationError as e:
                raise e
            else:
                obj.__dict__[self.label] = value

    def validate(self, value):
        """
        Validate fields
        """
        pass

    def to_python(self, value):
        return value


class CharField(Field):
    def __init__(self, strip=True, empty_value='', *args, **kwargs):
        self.strip = strip
        self.empty_value = empty_value
        super(CharField, self).__init__(*args, **kwargs)

    def to_python(self, value):
        if value not in self.empty_values:
            if self.strip and isinstance(value, basestring):
                value = value.strip()
        else:
            value = self.empty_value
        return value

    def validate(self, value):
        value = self.to_python(value)
        if not isinstance(value, basestring):
            raise ValidationError('{} must be string'.format(value))


class ArgumentsField(Field):
    """
        A Field that contains the logic of multiple Fields.
    """

    def __init__(self, *args, **kwargs):
        super(ArgumentsField, self).__init__(*args, **kwargs)
        self.empty_values = [{}]

    def validate(self, value):
        if not isinstance(value, dict):
            raise ValidationError('must be dict or tuple')


class EmailField(CharField):

    def validate(self, value):
        super(EmailField, self).validate(value)
        if EMAIL_RE.match(str(value)) is None:
            raise ValidationError('Invalid email address: {}'.format(value))


class PhoneField(Field):

    def validate(self, value):
        if not (isinstance(value, int) or isinstance(value, basestring)):
            raise ValidationError('{} must be integer or string'.format(value))
        if PHONE_RE.match(str(value)) is None:
            raise ValidationError('{} incorrect phone format number'.format(value))


class DateField(Field):

    def validate(self, value):
        try:
            datetime.datetime.strptime(value, DATE_FORMAT_RE)
        except ValueError:
            raise ValidationError('{} incorrect date format'.format(value))


class BirthDayField(DateField):

    def validate(self, value):
        super(BirthDayField, self).validate(value)
        birthday = datetime.datetime.strptime(value, DATE_FORMAT_RE)
        now = datetime.datetime.now()
        if now - birthday > YEAR_DELTA_70:
            raise ValidationError('Incorrect birthday date')


class GenderField(Field):
    def validate(self, value):
        if value not in [0, 1, 2]:
            raise ValidationError('{} is incorrect gender value'.format(value))
        return value


class ClientIDsField(Field):

    def validate(self, value):
        if not isinstance(value, (list, tuple)) or isinstance(value, basestring):
            raise ValidationError('ClientIDs must be list or tuple')
        for el in value:
            if not isinstance(el, int):
                raise ValidationError('ClientIDs object must be a integer')
        return value
