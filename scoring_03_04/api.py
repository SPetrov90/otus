#!/usr/bin/env python
# -*- coding: utf-8 -*-

import abc
import datetime
import hashlib
import json
import logging
import scoring
import uuid
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
from optparse import OptionParser

from fields import (ArgumentsField, BirthDayField, CharField, ClientIDsField, DateField,
                    EmailField, Field, GenderField, PhoneField, ValidationError)
from store import RedisStore

SALT = 'Otus'
ADMIN_LOGIN = 'admin'
ADMIN_SALT = '42'
OK = 200
BAD_REQUEST = 400
FORBIDDEN = 403
NOT_FOUND = 404
INVALID_REQUEST = 422
INTERNAL_ERROR = 500
ERRORS = {
    BAD_REQUEST: 'Bad Request',
    FORBIDDEN: 'Forbidden',
    NOT_FOUND: 'Not Found',
    INVALID_REQUEST: 'Invalid Request',
    INTERNAL_ERROR: 'Internal Server Error',
}
UNKNOWN = 0
MALE = 1
FEMALE = 2
GENDERS = {
    UNKNOWN: 'unknown',
    MALE: 'male',
    FEMALE: 'female',
}


class DeclarativeFieldsMetaclass(abc.ABCMeta):

    # only for collect names of declared fields
    def __new__(mcs, name, bases, attrs):
        current_fields = []
        for key, value in list(attrs.items()):
            if isinstance(value, Field):
                current_fields.append(key)
                value.label = key
        attrs['declared_fields'] = frozenset(current_fields)
        return super(DeclarativeFieldsMetaclass, mcs).__new__(mcs, name, bases, attrs)


class BaseRequest(object):
    __metaclass__ = DeclarativeFieldsMetaclass

    def __init__(self, data=None):
        self.data = data or {}
        self.errors = {}

        for name in self.declared_fields:
            value = self.data.get(name)
            try:
                setattr(self, name, value)
            except ValidationError as e:
                self.errors.update({name: e.message})

    def is_valid(self):
        return not self.errors


class ClientsInterestsRequest(BaseRequest):
    client_ids = ClientIDsField(required=True)
    date = DateField(required=False, nullable=True)

    def get_context(self):
        return {'nclients': len(self.client_ids) if self.client_ids else 0}

    def validate(self):
        pass


class OnlineScoreRequest(BaseRequest):
    first_name = CharField(required=False, nullable=True)
    last_name = CharField(required=False, nullable=True)
    email = EmailField(required=False, nullable=True)
    phone = PhoneField(required=False, nullable=True)
    birthday = BirthDayField(required=False, nullable=True)
    gender = GenderField(required=False, nullable=True)

    def validate(self):
        """
        Check pairs
        """
        contacts_data = self.phone and self.email
        name_data = self.first_name and self.last_name
        other_data = self.birthday and self.gender

        if not(contacts_data or name_data or other_data):
            self.errors.update({
                "there is not enough data":
                    "at least one of the pairs (phone-email), (first_name-last_name), (birthday-gender) is required"})

    def get_context(self):
        return {'has': [field for field, value in self.data.items() if value]}


class MethodRequest(BaseRequest):
    account = CharField(required=False, nullable=True)
    login = CharField(required=True, nullable=True)
    token = CharField(required=True, nullable=True)
    arguments = ArgumentsField(required=True, nullable=True)
    method = CharField(required=True, nullable=False)

    @property
    def is_admin(self):
        return self.login == ADMIN_LOGIN


def check_auth(request):
    if request.login == ADMIN_LOGIN:
        digest = hashlib.sha512(datetime.datetime.now().strftime(
            '%Y%m%d%H') + ADMIN_SALT).hexdigest()
    else:
        digest = hashlib.sha512(
            request.account + request.login + SALT).hexdigest()
    if digest == request.token:
        return True
    return False


def online_score_handler(request, ctx, store=None):
    if ctx.get("is_admin", None):
        score = 42
    else:
        kwargs = {
            "first_name": request.first_name,
            "last_name": request.last_name,
            "birthday": request.birthday,
            "gender": request.gender,
        }
        score = scoring.get_score(store, request.phone, request.email, **kwargs)
        ctx.update(request.get_context())
    return score, OK


def clients_interests_handler(request, ctx, store=None):
    response, code = {}, None

    client_ids = request.client_ids
    for id in client_ids:
        response.update({str(id): scoring.get_interests(store, id)})
    ctx.update(request.get_context())
    code = OK

    return response, code


def formatted_errors(errors_dict):
    return ", ".join(['{}="{}"'.format(key, value) for key, value in errors_dict.iteritems()])


def scoring_api_handler(request, ctx, store):
    mr = MethodRequest(request['body'])
    if not mr.is_valid():
        return formatted_errors(mr.errors), INVALID_REQUEST

    if check_auth(mr):
        methods = {
            "online_score": [OnlineScoreRequest, online_score_handler],
            "clients_interests": [ClientsInterestsRequest, clients_interests_handler],
        }
        ctx.update({'is_admin': mr.is_admin})

        if mr.method in methods:
            # get data method for handling
            dm = methods[mr.method][0](mr.arguments)
            dm.validate()
            if not dm.is_valid():
                return formatted_errors(dm.errors), INVALID_REQUEST

            return methods[mr.method][1](dm, ctx, store)
        else:
            return 'Incorrect api method', INVALID_REQUEST
    return "Forbidden", FORBIDDEN


class MainHTTPHandler(BaseHTTPRequestHandler):
    router = {
        'scoring': scoring_api_handler
    }
    store = RedisStore(host='localhost', port=6379, db=1, retry=5)

    def get_request_id(self, headers):
        return headers.get('HTTP_X_REQUEST_ID', uuid.uuid4().hex)

    def do_POST(self):
        response, code = {}, OK
        context = {'request_id': self.get_request_id(self.headers)}
        request = None
        try:
            data_string = self.rfile.read(int(self.headers['Content-Length'])).decode('utf-8')
            request = json.loads(data_string)
        except:
            code = BAD_REQUEST

        if request:
            path = self.path.strip('/')
            logging.info('%s: %s %s' %
                         (self.path, data_string, context['request_id']))
            if path in self.router:
                try:
                    response, code = self.router[path](
                        {'body': request, 'headers': self.headers}, context, self.store)
                except Exception, e:
                    logging.exception('Unexpected error: %s' % e)
                    code = INTERNAL_ERROR
            else:
                code = NOT_FOUND

        self.send_response(code)
        self.send_header('Content-Type', 'application/json')
        self.end_headers()
        if code not in ERRORS:
            r = {'response': response, 'code': code}
        else:
            r = {'error': response or ERRORS.get(
                code, 'Unknown Error'), 'code': code}
        context.update(r)
        logging.info(context)
        self.wfile.write(json.dumps(r))
        return


if __name__ == '__main__':

    op = OptionParser()
    op.add_option('-p', '--port', action='store', type=int, default=8080)
    op.add_option('-l', '--log', action='store', default=None)
    (opts, args) = op.parse_args()
    logging.basicConfig(filename=opts.log, level=logging.INFO,
                        format='[%(asctime)s] %(levelname).1s %(message)s', datefmt='%Y.%m.%d %H:%M:%S')
    server = HTTPServer(('localhost', opts.port), MainHTTPHandler)
    logging.info('Starting server at %s' % opts.port)
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass
    server.server_close()
