# -*- coding: utf-8 -*-

import mock
import time
import pytest
import redis

from scoring_03_04.store import RedisStore


def _get_client(cls, request=None, **kwargs):
    params = {'host': 'localhost', 'port': 6379, 'db': 1, 'retry': 5}
    params.update(kwargs)
    client = cls(**params)
    is_connected = client.make_connection()
    if is_connected:
        client.connection.flushdb()
        if request:
            def teardown():
                if client.connection:
                    client.connection.flushdb()
                    client.connection.connection_pool.disconnect()
            request.addfinalizer(teardown)
        return client


@pytest.fixture()
def r(request, cache_expire=2):
    return _get_client(RedisStore, request, cache_expire=cache_expire)


class TestRedisStore(object):
    """
    Test for store based on redis
    """
    def test_set(self, r):
        assert (r.set('key', 'value')) is True

    def test_get(self, r):
        assert (r.set('key', 'value')) is True
        value = r.get('key')
        assert value == 'value'

    def test_get_no_key(self, r):
        value = r.get('key')
        assert value is None

    def test_set_cache(self, r):
        assert (r.cache_set('key', 'value')) is True

    def test_get_cache(self, r):
        assert (r.cache_set('key', 'value')) is True
        value = r.cache_get('key')
        assert value == 'value'

    def test_get_cache_no_key(self, r):
        value = r.cache_get('key')
        assert value is None

    def test_one_attempt_connect(self, r):
        assert (r.set('key', 'value')) is True
        r.get('key')
        assert r.connect_attempts == 1

    def test_redis_reconnect(self, r):
        assert r.connect_attempts == 1
        # miss connection
        r.connection = None
        assert r.set('key', 'value') is True
        assert r.connect_attempts == 2
        value = r.get('key')
        assert value == 'value'

    def test_redis_get_data_after_reconnect(self, r):
        assert r.set('key', 'value') is True
        assert r.connect_attempts == 1
        r.connection = None
        value = r.get('key')
        assert r.connect_attempts == 2
        assert value == 'value'

    def test_redis_reconnect_count(self, r):
        assert r.connect_attempts == 1
        # miss connection
        r.connection = None
        retry_count = r.retry_count
        with mock.patch('redis.StrictRedis', autospec=True) as init:
            init.side_effect = Exception()
            with pytest.raises(redis.exceptions.ConnectionError):
                r.set('key', 'value')

        assert r.retry_count == 0
        assert r.connect_attempts == retry_count + 1

    def test_cache_expire(self, r):
        assert (r.cache_set('key', 'value')) is True
        value = r.cache_get('key')
        assert value == 'value'
        time.sleep(2)
        value = r.cache_get('key')
        assert value is None
