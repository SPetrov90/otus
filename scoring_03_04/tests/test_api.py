# -*- coding: utf-8 -*-

import hashlib
import datetime
import pytest

from .utils import idfn_value

from scoring_03_04.api import ADMIN_SALT, FORBIDDEN, MALE, INVALID_REQUEST, OK, SALT


def _get_auth_token(login, account):
    if login == 'admin':
        digest = hashlib.sha512(datetime.datetime.now().strftime(
            '%Y%m%d%H') + ADMIN_SALT).hexdigest()
    else:
        digest = hashlib.sha512(
            account + login + SALT).hexdigest()
    return digest


class TestAPI(object):
    """
    Test api handling
    """

    @pytest.mark.parametrize(
        "value",
        [
            ({}, INVALID_REQUEST),
        ],
        ids=idfn_value)
    def test_request_empty(self, get_api_response, value):
        data_value, status_value = value
        response, code = get_api_response(data_value)
        assert status_value == code

    @pytest.mark.parametrize(
        "value",
        [
            (
                {
                    "account": "otus",
                    "login": "admin",
                    "method": "online_score",
                    "token": _get_auth_token("bad_token", "otus"),
                    "arguments": {}
                },
                FORBIDDEN
            ),
            (
                {
                    "account": "otus",
                    "login": "sergey",
                    "method": "clients_interests",
                    "token": _get_auth_token("bad_token", "otus"),
                    "arguments": {}
                },
                FORBIDDEN
            )
        ],
        ids=idfn_value)
    def test_request_bad_auth_response(self, get_api_response, value):
        data_value, status_value = value
        response, code = get_api_response(data_value)
        assert FORBIDDEN == code

    @pytest.mark.parametrize(
        "value",
        [
            (
                {
                    "account": "otus",
                    "login": "admin",
                    "method": "online_score",
                    "token": _get_auth_token("admin", "otus"),
                    "arguments":
                        {
                            "phone": "79000000000",
                            "email": "SPetrov@otus.ru",
                            "first_name": u"Сергей",
                            "last_name": u"Петров",
                            "birthday": "01.01.1990",
                            "gender": MALE
                        }
                },
                int(ADMIN_SALT),
                OK,
            ),
            (
                {
                    "account": "otus",
                    "login": "sergey",
                    "method": "online_score",
                    "token": _get_auth_token("sergey", "otus"),
                    "arguments":
                        {
                            "phone": "79000000000",
                            "email": "SPetrov@otus.ru",
                            "first_name": u"Сергей",
                            "last_name": u"Петров",
                            "birthday": "01.01.1990",
                            "gender": MALE
                        }
                },
                float(5),
                OK,
            )
        ],
        ids=idfn_value)
    def test_online_score_request_success_response(self, get_api_response, value):
        data_value, response_value, status_value = value
        response, code = get_api_response(data_value)
        assert response == response_value
        assert OK == code

    @pytest.mark.parametrize(
        "value",
        [
            (
                {
                    "account": "otus",
                    "method": "online_score",
                    "token": _get_auth_token("sergey", "otus"),
                    "arguments": {}
                },
                "login=\"Value is required\"",
                INVALID_REQUEST,
            ),
            (
                {
                    "login": "admin",
                    "account": "otus",
                    "token": _get_auth_token("sergey", "otus"),
                    "arguments": {}
                },
                "method",
                INVALID_REQUEST,
            ),
            (
                {
                    "login": "admin",
                    "account": "otus",
                    "method": "online_score",
                    "token": _get_auth_token("sergey", "otus"),
                },
                "arguments",
                INVALID_REQUEST,
            ),
        ],
        ids=idfn_value)
    def test_bad_method_request_error_response(self, get_api_response, value):
        data_value, error_key, status_value = value
        response, code = get_api_response(data_value)
        assert error_key in response
        assert status_value == code

    @pytest.mark.parametrize(
        "value",
        [
            (
                {
                    "account": "otus",
                    "login": "sergey",
                    "method": "online_score",
                    "token": _get_auth_token("sergey", "otus"),
                    "arguments":
                        {
                            "phone": "79000000000",
                            "first_name": u"Сергей",
                            "birthday": "01.01.1990",
                        }
                },
                "there is not enough data",
                INVALID_REQUEST,
            ),
            (
                {
                    "account": "otus",
                    "login": "sergey",
                    "method": "online_score",
                    "token": _get_auth_token("sergey", "otus"),
                    "arguments":
                        {
                            "email": "SPetrov@otus.ru",
                            "last_name": u"Петров",
                            "gender": MALE
                        }
                },
                "there is not enough data",
                INVALID_REQUEST,
            )
        ],
        ids=idfn_value)
    def test_online_score_request_no_pairs(self, get_api_response, value):
        data_value, error_key, status_value = value
        response, code = get_api_response(data_value)
        assert error_key in response
        assert status_value == code

    @pytest.mark.parametrize(
        "value",
        [
            (
                {
                    "account": "otus",
                    "login": "sergey",
                    "method": "clients_interests",
                    "token": _get_auth_token("sergey", "otus"),
                    "arguments": {}
                },
                'client_ids',
                'date',
                INVALID_REQUEST
            )
        ],
        ids=idfn_value)
    def test_clients_interests_bad_request_error_response(self, get_api_response, value):
        data_value, error_key, valid_empty_key, status_value = value
        response, code = get_api_response(data_value)
        assert error_key in response
        assert valid_empty_key not in response
        assert status_value == code

    @pytest.mark.parametrize(
        "value",
        [
            (
                {
                    "account": "otus",
                    "login": "sergey",
                    "method": "clients_interests",
                    "token": _get_auth_token("sergey", "otus"),
                    "arguments": {"client_ids": [1, 2, 3, 4, 5], "date": "20.07.2017"}
                },
                {"1": [], "2": [], "3": [], "4": [], "5": []},
                OK
            )
        ],
        ids=idfn_value)
    def test_clients_interests_request_success_no_data(self, get_api_response, value):
        data_value, response_data, status_value = value
        response, code = get_api_response(data_value)
        assert status_value == code
        assert response == response_data

    @pytest.mark.parametrize(
        "value",
        [
            (
                {
                    "account": "otus",
                    "login": "sergey",
                    "method": "clients_interests",
                    "token": _get_auth_token("sergey", "otus"),
                    "arguments": {"client_ids": [1, 2, 3, 4, 5], "date": "20.07.2017"}
                },
                {
                    "1": ['cars', 'otus'],
                    "2": ['travel', 'geek'],
                    "3": ['hi-tech', 'cinema'],
                    "4": ['sport', 'tv'],
                    "5": ['music', 'books']
                },
                OK
            )
        ],
        ids=idfn_value)
    def test_clients_interests_request_success_with_data(self, get_api_response_data, value):
        data_value, response_data, status_value = value
        response, code = get_api_response_data(data_value)
        assert status_value == code
        assert response == response_data
