# -*- coding: utf-8 -*-


def idfn_value(val):
    return "value=({0})".format(str(val))


class MockStore(object):
    """
    Mock store base on python dict
    """

    def __init__(self):
        self.store = {}

    def get(self, key):
        return self.store.get(key, None)

    def set(self, key, value):
        self.store.update({key: value})
        return self.store.get(key, None)

    def cache_get(self, key):
        return self.store.get(key, None)

    def cache_set(self, key, value, expire=None):
        # in mock storage expire not implements
        self.store.update({key: value})
        return self.store.get(key, None)
