# -*- coding: utf-8 -*-

import pytest

from scoring_03_04.fields import ArgumentsField, BirthDayField, CharField, ClientIDsField, DateField, \
    EmailField, Field, GenderField, PhoneField, ValidationError
from .utils import idfn_value


class TestFieldsField(object):

    @pytest.mark.parametrize(
        "value",
        [
            (None, 'Value is required'),
            ("", 'Value cant be blank'),
            ([], 'Value cant be blank'),
            ((), 'Value cant be blank'),
            ({}, 'Value cant be blank')
        ],
        ids=idfn_value)
    def test_field_required_nullable_error(self, value):
        value, err = value
        field = Field(required=True, nullable=False)
        with pytest.raises(ValidationError) as e:
            field.__set__(self, value)
        assert str(e.value.message) == err


class TestFieldsCharField(object):

    @pytest.mark.parametrize(
        "value",
        [
            (None, 'Value is required'),
            ("", 'Value cant be blank'),
            ("   ", 'Value cant be blank'),
            ([], 'Value cant be blank'),
            ((), 'Value cant be blank'),
            ({}, 'Value cant be blank')
        ],
        ids=idfn_value)
    def test_charfield_required_nullable_error(self, value):
        value, err = value
        field = CharField(required=True, nullable=False)
        with pytest.raises(ValidationError) as e:
            field.__set__(self, value)
        assert str(e.value.message) == err

    @pytest.mark.parametrize(
        "value",
        [
            (None, ""),
            ("", ""),
            ([], ""),
            ((), ""),
            ({}, "")
        ],
        ids=idfn_value)
    def test_charfield_required_nullable_success(self, value):
        value, result = value
        field = CharField(required=False, nullable=True)
        field.__set__(self, value)
        assert field.__get__(self) == result

    @pytest.mark.parametrize(
        "value",
        ['str', u'str' '345', '8-911-000-00-00', 'test@test.ru', ''],
        ids=idfn_value)
    def test_charfield_good_values(self, value):
        field = CharField()
        field.__set__(self, value)
        assert field.__get__(self) == value

    @pytest.mark.parametrize(
        "value",
        [['str1', 'str2'], 777, (1, 2), {'1': '1', '2': '2'}],
        ids=idfn_value)
    def test_charfield_bad_values(self, value):
        field = CharField()
        with pytest.raises(ValidationError) as e:
            field.__set__(self, value)
        assert str(e.value.message) == "{} must be string".format(value)


class TestFieldsArgumentsField(object):

    @pytest.mark.parametrize(
        "value",
        [
            {"account": "horns&hoofs", "login": "admin11", "method": "online_score"},
            {"phone": '79175002040', "email": "test@otus.ru", "first_name": "Петров", "last_name": "Сергей"},
            {}
        ],
        ids=idfn_value)
    def test_argumentsfield_good_values(self, value):
        field = ArgumentsField()
        field.__set__(self, value)
        assert field.__get__(self) == value

    @pytest.mark.parametrize(
        "value",
        [['str1', 'str2'], 777, (1, 2), 'str', ''],
        ids=idfn_value)
    def test_argumentsfield_bad_values(self, value):
        field = ArgumentsField()
        with pytest.raises(ValidationError) as e:
            field.__set__(self, value)
        assert str(e.value.message) == "must be dict or tuple"


class TestFieldsEmailField(object):

    @pytest.mark.parametrize(
        "value",
        ["spetrov@google.com", "otus@otus.ru", ''],
        ids=idfn_value)
    def test_emailfield_good_values(self, value):
        field = EmailField()
        field.__set__(self, value)
        assert field.__get__(self) == value

    @pytest.mark.parametrize(
        "value",
        ["spetrov@", "otus@otus", "@", "string"],
        ids=idfn_value)
    def test_emailfield_bad_values(self, value):
        field = EmailField()
        with pytest.raises(ValidationError) as e:
            field.__set__(self, value)
        assert str(e.value.message) == "Invalid email address: {}".format(value)


class TestFieldsPhoneField(object):

    @pytest.mark.parametrize(
        "value",
        ["70000000000", "71234567890", ''],
        ids=idfn_value)
    def test_phonefield_good_values(self, value):
        field = PhoneField()
        field.__set__(self, value)
        assert field.__get__(self) == value

    @pytest.mark.parametrize(
        "value",
        ["700000000", "7123456789076", "phone"],
        ids=idfn_value)
    def test_phonefield_bad_values(self, value):
        field = PhoneField()
        with pytest.raises(ValidationError) as e:
            field.__set__(self, value)
        assert str(e.value.message) == "{} incorrect phone format number".format(value)

    @pytest.mark.parametrize(
        "value",
        [['ih', 'jn'], {'knk': 'hii'}],
        ids=idfn_value)
    def test_phonefield_bad_values_2(self, value):
        field = PhoneField()
        with pytest.raises(ValidationError) as e:
            field.__set__(self, value)
        assert str(e.value.message) == "{} must be integer or string".format(value)


class TestFieldsDateField(object):

    @pytest.mark.parametrize(
        "value",
        ["01.01.1950", "31.12.2000", "20.5.2010", "5.5.2018", ''],
        ids=idfn_value)
    def test_datefield_good_values(self, value):
        field = DateField()
        field.__set__(self, value)
        assert field.__get__(self) == value

    @pytest.mark.parametrize(
        "value",
        ["str", "13.32.9999", "1.0.2001", "20.20.1800"],
        ids=idfn_value)
    def test_datefield_bad_values(self, value):
        field = DateField()
        with pytest.raises(ValidationError) as e:
            field.__set__(self, value)
        assert str(e.value.message) == "{} incorrect date format".format(value)


class TestFieldsBirthDateField(object):

    @pytest.mark.parametrize(
        "value",
        ["01.01.1950", "31.12.2000", "20.5.2010", "5.5.2018", ''],
        ids=idfn_value)
    def test_birthdatefield_good_values(self, value):
        field = BirthDayField()
        field.__set__(self, value)
        assert field.__get__(self) == value

    @pytest.mark.parametrize(
        "value",
        ["1.1.1900", "20.12.1800"],
        ids=idfn_value)
    def test_birthdatefield_bad_values(self, value):
        field = BirthDayField()
        with pytest.raises(ValidationError) as e:
            field.__set__(self, value)
        assert str(e.value.message) == "Incorrect birthday date"


class TestFieldsGenderField(object):

    @pytest.mark.parametrize(
        "value",
        [0, 1, 2, ''],
        ids=idfn_value)
    def test_genderfield_good_values(self, value):
        field = GenderField()
        field.__set__(self, value)
        assert field.__get__(self) == value

    @pytest.mark.parametrize(
        "value",
        ["rfr", "1", ["rc", "dcv"]],
        ids=idfn_value)
    def test_genderfields_bad_values(self, value):
        field = GenderField()
        with pytest.raises(ValidationError) as e:
            field.__set__(self, value)
        assert str(e.value.message) == "{} is incorrect gender value".format(value)


class TestFieldsClientIdsField(object):

    @pytest.mark.parametrize(
        "value",
        [[0], [1, 2, 3], [], ''],
        ids=idfn_value)
    def test_clientidsfield_good_values(self, value):
        field = ClientIDsField()
        field.__set__(self, value)
        assert field.__get__(self) == value

    @pytest.mark.parametrize(
        "value",
        [['a', 'b', 'c']],
        ids=idfn_value)
    def test_clientidsfield_bad_values(self, value):
        field = ClientIDsField()
        with pytest.raises(ValidationError) as e:
            field.__set__(self, value)
        assert str(e.value.message) == "ClientIDs object must be a integer"

    @pytest.mark.parametrize(
        "value",
        ["list", "1"],
        ids=idfn_value)
    def test_clientidsfield_bad_values(self, value):
        field = ClientIDsField()
        with pytest.raises(ValidationError) as e:
            field.__set__(self, value)
        assert str(e.value.message) == "ClientIDs must be list or tuple"
