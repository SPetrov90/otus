# -*- coding: utf-8 -*-

import pytest

from scoring_03_04 import api
from utils import MockStore


def _prepare_mock_store():
    store = MockStore()
    return store


def _prepare_mock_store_with_test_data():
    store = MockStore()

    interests = ["cars", "travel", "hi-tech", "sport", "music", "books", "tv", "cinema", "geek", "otus"]

    for i in range(5):
        store.set("i:%s" % str(i + 1), [interests[i], interests[-(i+1)]])

    return store


def _get_response(request):
    return api.scoring_api_handler({'body': request, 'headers': {}}, {}, _prepare_mock_store())


def _get_response_store_data(request):
    return api.scoring_api_handler({'body': request, 'headers': {}}, {}, _prepare_mock_store_with_test_data())


@pytest.fixture(scope='function')
def get_api_response():
    return _get_response


@pytest.fixture(scope='function')
def get_api_response_data():
    return _get_response_store_data
