#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import sys
import glob
import gzip
import time
import logging
import threading
import collections
from queue import Queue
from multiprocessing import Pool
from optparse import OptionParser

import memcache

from appsinstalled_pb2 import UserApps

NORMAL_ERR_RATE = 0.01
AppsInstalled = collections.namedtuple("AppsInstalled", ["dev_type", "dev_id", "lat", "lon", "apps"])
CACHE_TIME = 86400


class MemcachedClient(threading.Thread):
    """
    Thread Class for put messages in one memcached server selected by client device type
    """

    def __init__(self, addr, queue):
        super().__init__()
        self.queue = queue
        self.server = addr
        self.errors = 0
        self.processed = 0
        self.daemon = True
        self.expire = CACHE_TIME

    def run(self):
        client = memcache.Client([self.server], socket_timeout=2)

        while True:
            msg = self.queue.get()
            if msg == 'STOP':
                break
            ok = None
            for _ in range(3):
                ok = client.set(msg['key'], msg['data'], self.expire)
                if ok:
                    self.processed += 1
                    break
            if not ok:
                self.errors += 1


class Worker:
    """
    Class for handling inserting data in memcached servers from one file
    """

    def __init__(self, device_memc, dry):
        self.dry = dry
        self.device_memc = device_memc

    def __call__(self, fn):
        logging.info('Processing %s' % fn)

        processed = errors = 0
        memc_queues = {device_type: Queue(100)
                       for device_type in self.device_memc.keys()}
        memclients = {}
        for dt, addr in self.device_memc.items():
            mc = MemcachedClient(addr, memc_queues[dt])
            mc.start()
            memclients[dt] = mc

        fd = gzip.open(fn)
        for line in fd:
            processed += 1
            line = line.strip()
            if not line:
                continue

            appsinstalled = self.parse_appsinstalled(line)
            if not appsinstalled:
                errors += 1
                continue

            queue = memc_queues.get(appsinstalled.dev_type)
            if not queue:
                errors += 1
                logging.error("Unknown device type: %s in file: %s" % appsinstalled.dev_type, fn)
                continue

            data = self.get_appsinstalled_data(appsinstalled)
            if self.dry:
                pass
                logging.debug("Processing %s: %s" % (fn, str(data)))
            else:
                queue.put(data)

        for queue in memc_queues.values():
            queue.put('STOP')

        for mc in memclients.values():
            mc.join()
            errors += mc.errors
            processed += mc.processed

        if processed:
            err_rate = float(errors) / processed
            if err_rate < NORMAL_ERR_RATE:
                logging.info("File %s. Acceptable error rate (%s). Successfull load" % (fn, err_rate))
            else:
                logging.error("File %s. High error rate (%s > %s). Failed load" % (fn, err_rate, NORMAL_ERR_RATE))
        else:
            logging.error("File %s. Cant processed any rows" % fn)
        fd.close()
        self.dot_rename(fn)

    def parse_appsinstalled(self, line):
        line_parts = line.decode().strip().split("\t")
        if len(line_parts) < 5:
            return
        dev_type, dev_id, lat, lon, raw_apps = line_parts
        if not dev_type or not dev_id:
            return
        try:
            apps = [int(a.strip()) for a in raw_apps.split(",")]
        except ValueError:
            apps = [int(a.strip()) for a in raw_apps.split(",") if a.isidigit()]
            logging.info("Not all user apps are digits: `%s`" % line)
        try:
            lat, lon = float(lat), float(lon)
        except ValueError:
            logging.info("Invalid geo coords: `%s`" % line)
        return AppsInstalled(dev_type, dev_id, lat, lon, apps)

    def get_appsinstalled_data(self, appsinstalled):
        ua = UserApps()
        ua.lat = appsinstalled.lat
        ua.lon = appsinstalled.lon
        key = "%s:%s" % (appsinstalled.dev_type, appsinstalled.dev_id)
        ua.apps.extend(appsinstalled.apps)
        packed = ua.SerializeToString()
        return key, packed

    def dot_rename(self, path):
        head, fn = os.path.split(path)
        os.rename(path, os.path.join(head, "." + fn))


def main(options):
    device_memc = {
        "idfa": options.idfa,
        "gaid": options.gaid,
        "adid": options.adid,
        "dvid": options.dvid,
    }
    file_names = glob.glob(options.pattern)
    workers_pool = Pool(options.workers)
    workers_pool.map(Worker(device_memc, options.dry), sorted(file_names))
    logging.info("processed (%s) files" % len(file_names))

def prototest():
    sample = "idfa\t1rfw452y52g2gq4g\t55.55\t42.42\t1423,43,567,3,7,23\ngaid\t7rfw452y52g2gq4g\t55.55\t42.42\t7423,424"
    for line in sample.splitlines():
        dev_type, dev_id, lat, lon, raw_apps = line.strip().split("\t")
        apps = [int(a) for a in raw_apps.split(",") if a.isdigit()]
        lat, lon = float(lat), float(lon)
        ua = UserApps()
        ua.lat = lat
        ua.lon = lon
        ua.apps.extend(apps)
        packed = ua.SerializeToString()
        unpacked = UserApps()
        unpacked.ParseFromString(packed)
        assert ua == unpacked


if __name__ == '__main__':
    op = OptionParser()
    op.add_option("-t", "--test", action="store_true", default=False)
    op.add_option("-l", "--log", action="store", default=None)
    op.add_option("--dry", action="store_true", default=False)
    op.add_option("--workers", action="store", type="int", default=3)
    op.add_option("--pattern", action="store", default="./data/appsinstalled/*.tsv.gz")
    op.add_option("--idfa", action="store", default="127.0.0.1:33013")
    op.add_option("--gaid", action="store", default="127.0.0.1:33014")
    op.add_option("--adid", action="store", default="127.0.0.1:33015")
    op.add_option("--dvid", action="store", default="127.0.0.1:33016")
    (opts, args) = op.parse_args()
    logging.basicConfig(filename=opts.log, level=logging.INFO if not opts.dry else logging.DEBUG,
                        format='[%(asctime)s] %(levelname).1s %(message)s', datefmt='%Y.%m.%d %H:%M:%S')
    if opts.test:
        prototest()
        sys.exit(0)

    logging.info("Memc loader started with options: %s" % opts)
    try:
        start = time.perf_counter()
        main(opts)
        end = time.perf_counter()
        logging.info("Completed in: {} sec".format(end - start))
    except Exception as e:
        logging.exception("Unexpected error: %s" % e)
        sys.exit(1)
