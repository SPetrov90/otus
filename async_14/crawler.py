"""
Crawler goal - site https://news.ycombinator.com.
Start work from site main page.
Crawler download top 30 news with comments link pages and waiting for new ones.
New folder is created for each news item.
"""

import asyncio
from concurrent.futures import ThreadPoolExecutor
import logging
import multiprocessing
from optparse import OptionParser
import os
import re

import aiohttp
import async_timeout
from bs4 import BeautifulSoup

######CONSTANTS######

RPS_LIMIT = 4
MAIN_PAGE = 'https://news.ycombinator.com/'
MAIN_PAGE_NEWS_URL_SELECTOR = 'td.subtext span.age a'
NEWS_PAGE_URL_SELECTOR = 'td.title > a'
COMMENTS_PAGE_URL_SELECTOR = 'div.comment a[rel="nofollow"]'
MAX_DIR_NAME_LENGTH = 40

#####################

def get_args():
    """
    parse starting config
    :return: tuple (options, args)
    """
    op = OptionParser()
    op.add_option("-p", "--parsers", action="store", type="int", default=3)
    op.add_option("-d", "--downloaders", action="store", type="int", default=5)
    op.add_option("-l", "--logfile", action="store", default=None)
    op.add_option("-t", "--timeout", action="store", type="int", default=10)
    op.add_option("-r", "--refresh_timeout", action="store", type="int", default=60)
    op.add_option("-b", "--basedir", action="store", default='download')
    return op.parse_args()

async def check_main_page_coroutine(news_queue, timeout, refresh_timeout):
    processed_news = set()
    while True:
        logging.info("check main page")
        # put in news_queue urls
        await handle_main_page_news(news_queue, processed_news, timeout)
        # wait until all news was handling
        await news_queue.join()
        logging.info("check main page end")
        await asyncio.sleep(refresh_timeout)

async def handle_one_news_coroutine(news_queue, download_queue, loop, pool, sem_rps, base_dir, timeout):
    while True:
        link = await news_queue.get()
        link = make_full_url(MAIN_PAGE, link)
        async with sem_rps:
            logging.info('handling link: {}'.format(link))
            content = await get_url_bs_content(link, timeout)
            if content:
                news_url_tag = content.select(NEWS_PAGE_URL_SELECTOR)[0]
                link_text = news_url_tag.get_text().lower().strip()
                news_folder_name = slugify_name(link_text)
                try:
                    fut = loop.run_in_executor(pool, create_dir, base_dir, news_folder_name)
                    await asyncio.wait_for(fut, timeout=timeout)
                    news_full_path = fut.result()
                    logging.info("Create folder: %s" % news_full_path)
                except asyncio.TimeoutError:
                    logging.exception("Timeout error for create folder: %s" % news_folder_name)
                    continue
                except Exception as err:
                    logging.exception('Error while create folder: %s, %s' % news_folder_name, err)
                    continue

                news_url = make_full_url(MAIN_PAGE, news_url_tag.get('href'))
                news_full_name = os.path.join(news_full_path, 'index.html')

                await download_queue.put((news_full_name, news_url))
                logging.debug('put news url: %s to download queue' % news_url)

                comments_url_tags = content.select(COMMENTS_PAGE_URL_SELECTOR)
                index = 0
                for url_tag in comments_url_tags:
                    index += 1
                    file_name = 'comment_{}.html'.format(index)
                    comment_full_name = os.path.join(news_full_path, file_name)
                    comment_url = make_full_url(MAIN_PAGE, url_tag.get('href'))
                    await download_queue.put((comment_full_name, comment_url))
                    logging.debug('put comment url: %s to download queue' % comment_url)
            news_queue.task_done()

async def download_page_coroutine(download_queue, loop, pool, timeout):
    while True:
        full_fn, link = await download_queue.get()
        async with aiohttp.ClientSession() as session:
            content = await fetch_url_content(session, link)

        if content:
            try:
                fut = loop.run_in_executor(pool, write_to_file, full_fn, content)
                await asyncio.wait_for(fut, timeout=timeout)
                logging.info("save file: %s" % full_fn)
            except asyncio.TimeoutError:
                logging.exception("Timeout error for save file: %s" % full_fn)
            except Exception as err:
                logging.exception('Error while save file: %s, %s' % full_fn, err)
            finally:
                download_queue.task_done()

async def fetch_url_content(session: aiohttp.ClientSession, url: str, retries=5, retry_timeout=2):
    while retries > 0:
        try:
            response = await session.get(url)
            if response.status == 200 and ('text/html' in response.headers.get('content-type')):
                return await response.text()
            retries -= 1
            await asyncio.sleep(retry_timeout)
            continue
        except asyncio.CancelledError:
            logging.error('Fetching url: %s was canceled' % url)
            break
        except:
            logging.exception('Error getting: %s. Retrying...%s' % url, retries)
            retries -= 1
            await asyncio.sleep(retry_timeout)
            continue
    return ''

async def get_url_bs_content(url, timeout):
    bs_content = None
    try:
        async with async_timeout.timeout(timeout):
            async with aiohttp.ClientSession() as session:
                content = await fetch_url_content(session, url)
                bs_content = BeautifulSoup(content, 'html.parser')
    except asyncio.TimeoutError:
        logging.exception("timeout error for site request main page")
    except Exception as err:
        logging.exception("Unexpected error error for site request main page %s" % err)

    if not bs_content or not bs_content.get_text():
        logging.error('Can not get data from site main page')
        return None
    return bs_content

def make_full_url(prefix: str, href: str):
    if not href.startswith('http'):
        return os.path.join(prefix, href)
    return href

def slugify_name(link_text):
    return re.sub(r'(\W|\s)+', '_', link_text)[:MAX_DIR_NAME_LENGTH]

def create_dir(basedir, name):
    dir_name = os.path.join(basedir, name)
    logging.info("Create new folder: %s for a piece of news" % dir_name)
    os.makedirs(dir_name, exist_ok=True)
    return dir_name

def write_to_file(path, content):
    with open(path, 'w') as file:
        file.write(content)

async def handle_main_page_news(news_queue, processed_news, timeout):
    content = await get_url_bs_content(MAIN_PAGE, timeout)
    if content:
        news_comments_page_links = content.select(MAIN_PAGE_NEWS_URL_SELECTOR)
        for link in news_comments_page_links:
            href = link.get('href')
            if href and href not in processed_news:
                processed_news.add(href)
                await news_queue.put(href)
                logging.debug('put in news_queue url: %s' % href)

def main(config):
    """
    :param config: config with options getting with optparse
    """

    loop = asyncio.get_event_loop()
    news_queue = asyncio.Queue(30)
    download_queue = asyncio.Queue()
    pool = ThreadPoolExecutor(max_workers=multiprocessing.cpu_count())
    sem_rps = asyncio.Semaphore(RPS_LIMIT)

    loop.create_task(check_main_page_coroutine(news_queue, config.timeout, config.refresh_timeout))
    for _ in range(config.parsers):
        loop.create_task(handle_one_news_coroutine(
                news_queue, download_queue, loop, pool,
                sem_rps, config.basedir, config.timeout
            )
        )
    for _ in range(config.downloaders):
        loop.create_task(download_page_coroutine(download_queue, loop, pool, config.timeout))
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        logging.info('Key Interrupt, exiting....')
        for task in asyncio.Task.all_tasks():
            task.cancel()
    finally:
        loop.close()


if __name__ == '__main__':
    (opts, args) = get_args()
    logging.basicConfig(filename=opts.logfile, level=logging.INFO,
                        format='[%(asctime)s] %(funcName)s: %(levelname).1s %(message)s',
                        datefmt='%Y.%m.%d %H:%M:%S')
    try:
        main(opts)
    except Exception as err:
        logging.error('Unexpected error: %s' % err)
