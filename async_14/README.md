# Async crawler (news.ycombinator.com)

### Requirements:
 - Python 3.6
 - aiohttp
 - async_timeout
 - beautifulsoup4


## Usage:

#### Go to project dir:
```
cd %path_to_module_dir%
pip install requirement.txt (better use it with virtual env)
```

#### Run:

```
python crawler.py
# or with args
python crawler.py 
		 ["-p", "--parsers", default=3]
		 ["-d", "--downloaders", default=5]
		 ["-l", "--logfile", default=None] 
		 ["-t", "--timeout", default=10]
		 ["-r", "--refresh_timeout", default=60]
		 ["-b", "--basedir", default='download']
```
