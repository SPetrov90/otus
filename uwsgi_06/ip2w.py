import os
import requests
import logging
import json
import socket

config = {
    'API_WEATHER_URL': 'http://api.openweathermap.org/data/2.5/weather',
    'API_WEATHER_KEY': os.environ.get("API_WEATHER_KEY"),
    'API_IP_URL': 'https://ipinfo.io',
    'REQUEST_TIMEOUT': 1,
    'LOG_PATH': None,
    'LOG_LEVEL': logging.DEBUG
}

logging.basicConfig(filename=config.get('LOG_PATH'), level=config.get('LOG_LEVEL'),
                    format='[%(asctime)s] %(levelname).1s %(message)s', datefmt='%Y.%m.%d %H:%M:%S')


def check_ip(addr):
    try:
        socket.inet_aton(addr)
        return True
    except socket.error:
        return False

def request_process(url, **params):
    attempts = 3
    while attempts > 0:
        try:
            response = requests.get(url, params={**params}, timeout=config.get('REQUEST_TIMEOUT'))
            json = response.json()
            response.close()
            logging.debug('returned request json: %s' % json)
            return json
        except requests.RequestException as err:
            attempts -= 1
            logging.exception('Request error to url: %s, %s' % url, err)
            continue

def get_ip_detail(ip):
    logging.debug('Get info for ip: %s' % ip)
    url = '{}/{}/json'.format(config.get('API_IP_URL'), ip)
    return request_process(url)


def get_weather_info_by_loc(lat, lon):
    logging.debug('Get weather for coords:  %s, %s' % (lat, lon))
    params = {
        'lat': lat,
        'lon': lon,
        'appid': config.get('API_WEATHER_KEY')
    }
    return request_process(config.get('API_WEATHER_URL'), params=params,)


def error_response(status_code, error, start_response):
    logging.debug(error)
    start_response(status_code, [('Content-Type', 'application/json')])
    return [json.dumps({'error': error}).encode('utf-8')]

def get_loc_by_ip_info(ip_info):
    lat, lon = ip_info.get('loc').split(',')
    logging.debug('lat %s, long %s' % (lat, lon))
    return float(lat), float(lon)

def get_weather_data(weather_info):
    return json.dumps({weather_info.get('name'): weather_info.get('weather') }).encode('utf-8')

def application(env, start_response):
    try:
        url_paths = env.get('REQUEST_URI').split('/')
        list(filter(lambda a: a != '', url_paths))
        logging.debug('url_paths: %s' % url_paths)

        if len(url_paths) != 2:
            return error_response('400 Bad Request', 'url must have format /ip2w/<ip>', start_response)

        ip_addr = url_paths[-1]
        if not check_ip(ip_addr):
            return error_response('400 Bad Request', 'incorrect IP <%s>' % ip_addr, start_response)

        ip_info = get_ip_detail(ip_addr)
        if ip_info:
            lat, lon = get_loc_by_ip_info(ip_info)
            weather_info = get_weather_info_by_loc(lat, lon)

            if weather_info and weather_info.get('cod') == 200:
                weather = get_weather_data(weather_info)
                start_response('200 OK', [('Content-Type', 'application/json')])
                return [weather]
            else:
                return error_response('200 OK', 'No weather data for IP address', start_response)
        else:
            return error_response('200 OK', 'Cant get data for ip: %s' % ip_addr, start_response)

    except Exception as err:
        logging.error(err)
        return error_response('500 Internal Server Error', 'Internal server error', start_response)
