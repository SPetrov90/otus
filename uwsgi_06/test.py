# -*- coding: utf-8 -*-
import unittest

from ip2w import application

def mock_start_response(status, headers):
    pass

class IP2WTest(unittest.TestCase):

    def test_weather_corrent_data(self):
        conf = {'REQUEST_URI': 'ip2w/93.185.18.80'}
        result = application(conf, mock_start_response)
        self.assertIn('Velikiye Luki', str(result))

    def test_weather_incorrent_path(self):
        conf = {'REQUEST_URI': 'ip2w/93.185.18.80/fake'}
        result = application(conf, mock_start_response)
        self.assertIn('url must have format /ip2w/<ip>', str(result))

    def test_weather_incorrent_ip(self):
        conf = {'REQUEST_URI': 'ip2w/not_ip_value'}
        result = application(conf, mock_start_response)
        self.assertIn('incorrect IP <not_ip_value>', str(result))

if __name__ == '__main__':
    ip2wTestSuite = unittest.TestSuite()
    ip2wTestSuite.addTest(unittest.makeSuite(IP2WTest))

    runner = unittest.TextTestRunner(verbosity=2)
    runner.run(ip2wTestSuite)
    unittest.main()
