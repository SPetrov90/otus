# Otus HTTP Web Server

### Features:
 - implements the HTTP protocol for GET and HEAD methods;
 - can be expansion to multiple workers (threads);
 - return index.html as index of directory
 - allows response codes: 200, 400, 403, 404, 405, 413, 505;
 - allows response headers: Date, Server, Content-Length, Content-Type, Connection;
 - allows content types: .html, .css, .js, .jpg, .jpeg, .png, .gif, .swf;
 - allows spaces and %XX in file's names...

### Requirements:
 - Python 3


## Usage:

#### Go to project dir:
```
cd %path_to_module_dir%
```

#### Run server:

```
make run
# or with args
make run 
		 [post='server_port'] 
		 [ip='server_ip]
		 [workers='count_of_workers'] 
		 [documentroot='place_for_static']
		 [log='path_to_write_logs']

```

## Tests:

####  Unit testing:

Tests for testing server from [https://github.com/s-stupnikov/http-test-suite](https://github.com/s-stupnikov/http-test-suite)

For running tests use (server must bu running):
```
make test
```

####  Result of AB testing:

Server Software:        OtusWevServer  
Server Hostname:        127.0.0.1  
Server Port:            80`

Document Path:          /httptest/dir2/index.html  
Document Length:        0 bytes  
Concurrency Level:      100

Time taken for tests:   20.958 seconds  
Complete requests:      50000  
Failed requests:        0   
Total transferred:      8950000 bytes  
HTML transferred:       0 bytes  
Requests per second:    2385.70 (#/sec) (mean)  
Time per request:       41.916 (ms) (mean)  
Time per request:       0.419 (ms) (mean, across all concurrent requests)  
Transfer rate:          417.03 (Kbytes/sec) received  

Connection Times (ms)  

  
| name         |min|mean| median | max |
| -------------|---|----|--------|------|
| Connect      |0  | 0  | 0      |301  |
| Process      |1  | 42 | 48.8   |433  |
| Waiting      |1  | 41 | 48.5   |433  |
| Total        |6  | 42 | 48.7   |433  |
  
Percentage of the requests served within a certain time (ms)
  
|percent | requests|
|--------|---------|
|  50%   |   33   |
|  66%   |   34   |
|  75%   |   35   |
|  80%   |   35   |
| 90%    |   37   |
|  95%   |   40   |
|  98%   |   300  |
|  99%   |   327  |
| 100%   |   433  |
