
from argparse import ArgumentParser
import email.utils
import logging
import mimetypes
import os
import posixpath
import socket
import threading
import time
import urllib.parse

CONFIG = {
    'IP': '0.0.0.0',
    'PORT': '80',
    'WORKERS': 10,
    'DOCUMENT_ROOT': 'www',
    'LOG_DIR': None,
}

OK = 200
NOT_FOUND = 404
FORBIDDEN = 403
BAD_REQUEST = 400
NOT_ALLOWED = 405
REQUEST_ENTITY_TOO_LARGE = 413
INTERNAL_SERVER_ERROR = 500
HTTP_VERSION_NOT_SUPPORTED = 505

DEFAULT_ERROR_MESSAGE = """\
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
        "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
        <title>Error response</title>
    </head>
    <body>
        <h1>Error response</h1>
        <p>Error code: {}</p>
        <p>Message: {}.</p>
    </body>
</html>
"""


class HttpRequestHandler:
    """
    Handler of the one request
    """

    index = "index.html"
    default_request_version = "HTTP/1.1"
    timeout = 6
    allows_types = ['.html', '.css', '.js', '.jpg', '.jpeg', '.png', '.gif', '.swf']
    default_mimetype = 'text/html'

    def __init__(self, request, client_address, server):
        self.connection = request
        self.client_address = client_address
        self.server = server
        self.setup()

        # request opts
        self.headers = {}
        self.error = None

        # response opts
        self.response = b''

    def setup(self):
        """
        Set timeout for request
        :return:
        """
        if self.timeout is not None:
            self.connection.settimeout(self.timeout)

    def handle_request(self):
        """
        Main function for handling request
        :return:
        """
        try:
            r = self.read()
            if self.error:
                self.set_response_with_err(self.error)
                return False

            self.parse_request(r)
            if self.error:
                self.set_response_with_err(self.error)
                return

            mname = 'do_' + self.headers['command']
            logging.debug("request method: {}".format(mname))
            if not hasattr(self, mname):
                self.error = (NOT_ALLOWED, 'Unsupported method ({})'.format(self.headers['command']))
                self.set_response_with_err(self.error)
                return
            method = getattr(self, mname)
            method()
        finally:
            self.finish()

    def do_GET(self):
        """
        handler for GET requests
        :return: 
        """
        path = self.translate_path(self.headers['path'])
        content_type = self.get_content_type(path)
        body = self.read_file_by_path(path)
        if not body:
            self.error = (NOT_FOUND, 'Page not found')
            self.set_response_with_err(self.error)
            return False
        headers = self.get_response_headers(OK, len(body), content_type)
        self.response = headers.encode() + body + b'\r\n\r\n'
        logging.debug('GET method response headers: {}'.format(headers))

    def do_HEAD(self):
        """
        handler for HEAD requests
        :return: 
        """
        path = self.translate_path(self.headers['path'])
        content_type = self.get_content_type(path)
        try:
            size = os.path.getsize(path)
        except FileNotFoundError:
            self.error = (NOT_FOUND, 'Page not found')
            self.set_response_with_err(self.error)
            return False

        headers = self.get_response_headers(OK, size, content_type)
        self.response = headers.encode() + b'\r\n\r\n'
        logging.debug('HEAD method response headers: {}'.format(headers))

    def read(self, buf_size=1024):
        """
        Read from socket while not exist "http end structure" or size more than limit
        :return request bytes string
        """
        maxsize = 65536
        data = b''
        req_end = b'\r\n\r\n'
        while 1:
            buf = self.connection.recv(buf_size)
            data += buf
            if len(data) > maxsize:
                # 413 Payload Too Large
                self.error = (REQUEST_ENTITY_TOO_LARGE, 'Payload Too Large')
                break
            if data.endswith(req_end):
                break
            if not data:
                logging.DEBUG('connection broken')
                raise RuntimeError("connection broken")
        return data

    def parse_request(self, data):
        """
        Parsing request and set headers or errors
        :param data: request bytes string
        :return:
        """

        # стандартная кодировка запроса
        request = str(data, 'iso-8859-1')
        requestline = request.rstrip('\r\n')
        headerslist = requestline.split('\r\n')

        try:
            firstline, add_headers = headerslist[0], headerslist[1:]
            words = firstline.split()
        except Exception as e:
            self.error = (BAD_REQUEST, "Bad request: ({})".format(requestline))
            return False

        command, path, version = '', '', self.default_request_version
        if len(words) == 3:
            command, path, version = words

            try:
                if version[:5] != 'HTTP/':
                    raise ValueError
                base_version_number = version.split('/', 1)[1]
                version_number = base_version_number.split(".")
                if len(version_number) != 2:
                    raise ValueError
                version_number = int(version_number[0]), int(version_number[1])
            except (ValueError, IndexError):
                self.error = (BAD_REQUEST, "Bad request version ({})".format(version))
                return False
            if version_number >= (2, 0):
                self.error = (HTTP_VERSION_NOT_SUPPORTED, "Invalid HTTP version ({})".format(base_version_number))
                return False

        elif len(words) == 2:
            command, path = words

        else:
            self.error = (BAD_REQUEST, "Bad request: ({})".format(requestline))
            return False

        headers = dict()
        headers['command'] = command
        headers['path'] = path
        headers['version'] = version

        for header in add_headers:
            key, value = header.split(': ')
            headers[key] = value.strip()

        self.headers = headers
        logging.debug('REQUEST HEADERS: {}'.format(headers))
        return False

    def set_response_with_err(self, err):
        """
        set response with error
        :param err: кортеж (код ошибки, текст ошибки)
        :return:
        """
        logging.debug('err {}'.format(err))
        status, text = err
        text = DEFAULT_ERROR_MESSAGE.format(status, text).encode()
        self.response = self.get_response(status, text)

    def get_content_type(self, path):
        """
        get content type from path
        :param path: file path
        :return: content-type
        """
        base, ext = posixpath.splitext(path)
        logging.debug(ext.lower())
        if ext.lower() in self.allows_types:
            logging.debug(mimetypes.types_map[ext.lower()])
            return mimetypes.types_map[ext.lower()]
        else:
            return self.default_mimetype

    def read_file_by_path(self, path):
        """
        return the contents of the file in bytes
        :param file path
        """
        b = b''
        try:
            logging.debug('open file from request: {}'.format(path))
            with open(path, 'rb') as f:
                b = f.read()
        except Exception as e:
            logging.debug("Err read file")
        return b

    def date_time_string(self, timestamp=None):
        """Return the current date and time formatted for a message header."""
        if timestamp is None:
            timestamp = time.time()
        return email.utils.formatdate(timestamp, usegmt=True)

    def translate_path(self, path):
        """
        matching path from request to path with document root
        """
        logging.debug('before translate PATH: {}'.format(path))
        # удаляем параметры запроса
        path = path.split('?', 1)[0]
        path = path.split('#', 1)[0]
        trailing_slash = path.rstrip().endswith('/')

        try:
            path = urllib.parse.unquote(path, errors='surrogatepass')
        except UnicodeDecodeError:
            path = urllib.parse.unquote(path)
        path = posixpath.normpath(path)

        path = self.server.root + path
        if os.path.isdir(path):
            path = os.path.join(path, self.index)
        # для файлов возвращаем / если он был в запросе
        elif trailing_slash:
            path += '/'

        logging.debug('after translate PATH: {}'.format(path))
        return path

    def get_response(self, status, body, content_type='text/html'):
        headers = self.get_response_headers(status, len(body), content_type)
        response = headers.encode() + body + b'\r\n\r\n'
        logging.debug('response headers: {}'.format(headers))
        return response

    def get_http_status(self, code, protocol):
        """
        Get response status
        :param code: status code
        :param protocol: http protocol version
        :return: response status he
        """
        templates = {
            OK: "<Protocol> 200 OK\r\n",
            FORBIDDEN: "<Protocol> 403 FORBIDDEN\r\n",
            NOT_FOUND: "<Protocol> 404 NOT FOUND\r\n",
            NOT_ALLOWED: "<Protocol> 405 METHOD NOT ALLOWED\r\n"
        }
        return templates[code].replace("<Protocol>", protocol)

    def get_response_headers(self, code, content_length, content_type):
        """ Generates HTTP response Headers."""
        h = ''
        try:
            h += self.get_http_status(code, self.headers['version'])
        except KeyError:
            raise ValueError('Unexpected response code: {}'.format(code))

        h += 'Date: ' + self.date_time_string() + '\r\n'
        h += 'Server: ' + self.server.name + '\r\n'
        h += 'Content-Length: {}\r\n'.format(content_length)
        h += 'Content-Type: {}\r\n'.format(content_type)
        h += 'Connection: close\n\n'

        return h

    def finish(self):
        """
        Send response and close socket
        """
        if self.response:
            try:
                self.connection.sendall(self.response)
            except socket.error:
                # A final socket error may have occurred here, such as
                # the local error ECONNABORTED.
                pass

        self.connection.close()


class OtusHttpServer:
    """
    HTTP Server class. Base on thread pool
    """

    request_queue_size = 100
    handler_class = HttpRequestHandler

    def __init__(self, config):

        self.root = config.get('DOCUMENT_ROOT')
        self.name = config.get('SERVER_NAME')
        self.workers_count = int(config.get('WORKERS'))
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.bind((config.get('HOST'), config.get('PORT')))
        self.server_address = self.socket.getsockname()
        self.workers = []

        logging.debug('Starting server {}'.format(self.server_address))

    def serve_forever(self):
        """
        Create and run workers for listen socket
        """
        try:
            for i in range(self.workers_count):
                t = threading.Thread(target=self._listen_socket, args=())
                t.start()
                self.workers.append(t)
        except Exception:
            self.server_close()
            raise

    def _listen_socket(self):
        """
        Main function for worker: lister socket, accepting connection, initial handler class
        """
        self.socket.listen(self.request_queue_size)
        ident = threading.get_ident()
        logging.info('Starting worker #{}'.format(ident))
        while True:
            try:
                request, client_address = self.socket.accept()
                logging.info("Get connection {} handling by worker #{}".format(client_address, ident))
            except OSError:
                logging.error("Error by worker #{}".format(ident))
                return
            handler = self.handler_class(request, client_address, self)
            handler.handle_request()

    def server_close(self):
        """
        Shutdown workers and close the server socket
        """
        for thread in self.workers:
            thread.join(1)
        self.socket.close()


def get_config_from_argparse():
    parser = ArgumentParser()
    parser.add_argument('-i', '--ip', default=CONFIG['IP'])
    parser.add_argument('-p', '--port', default=CONFIG['PORT'], type=int)
    parser.add_argument('-w', '--workers', default=CONFIG['WORKERS'], type=int)
    parser.add_argument('-r', '--documentroot', default=CONFIG['DOCUMENT_ROOT'])
    parser.add_argument('-l', '--log', action='store', default=CONFIG['LOG_DIR'])
    args = parser.parse_args()

    config = {
        'HOST': args.ip,
        'PORT': args.port,
        'WORKERS': args.workers,
        'DOCUMENT_ROOT': args.documentroot,
        'LOG_PATH': args.log,
        'SERVER_NAME': 'OtusWevServer',
    }

    return config


def main():
    try:
        config = get_config_from_argparse()

        logging.basicConfig(
            format='[%(asctime)s] %(levelname).1s %(message)s', datefmt='%Y.%m.%d %H:%M:%S',
            level=logging.ERROR, filename=config.get("LOG_PATH")
        )

        logging.debug('start server with config {}'.format(config))

        server = OtusHttpServer(config)
        server.serve_forever()

    except KeyboardInterrupt:
        logging.info("Stop server!")
        logging.shutdown()
        return
    except Exception as e:
        logging.exception("Server working error: {}".format(e))


if __name__ == '__main__':
    main()
